package com.example.tin.tothetube.model;

public class Arrival extends BaseModel {

    public String id;
    public Integer operationType;
    public String vehicleId;
    public String naptanId;
    public String stationName;
    public String lineId;
    public String lineName;
    public String platformName;
    public String direction;
    public String destinationNaptanId;
    public String destinationName;
    public String timestamp;
    public Integer timeToStation;
    public String currentLocation;
    public String towards;
    public Timing timing;
    public String expectedArrival;
    public String timeToLive;
    public String modeName;

}
