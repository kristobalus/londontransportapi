package com.example.tin.tothetube.repositories;

import com.example.tin.tothetube.model.Arrival;
import com.example.tin.tothetube.model.StopPoint;
import com.example.tin.tothetube.services.StopPointService;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

// StopPointRepository is an abstraction of the StopPointService
// it decouples the networking logic from application logic
public class StopPointRepository {

    private static StopPointRepository sInstance;

    public static StopPointRepository getInstance() {
        if (sInstance == null) {
            sInstance = new StopPointRepository();
        }
        return sInstance;
    }


    public List<StopPoint> getStopPointList(Double lat, Double lng, String stopType, Integer radius) throws IOException {

        Call<StopPoint.PaginatedResponse> call = StopPointService.getInstance().getByRadius(lat, lng, stopType, radius);
        Response<StopPoint.PaginatedResponse> response = call.execute();
        if (!response.isSuccessful()) {
            // to do error handling
        }
        StopPoint.PaginatedResponse wrapper = response.body();
        return wrapper.stopPoints;
    }

    public List<Arrival> getArrivalList(String stopPointId) throws IOException {

        Call<List<Arrival>> call = StopPointService.getInstance().getArrivalsById(stopPointId);

        Response<List<Arrival>> response = call.execute();
        if (!response.isSuccessful()) {
            // to do error handling
        }

        return response.body();
    }


}
