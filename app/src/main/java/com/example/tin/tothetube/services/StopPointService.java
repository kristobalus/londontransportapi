package com.example.tin.tothetube.services;

import com.example.tin.tothetube.BuildConfig;
import com.example.tin.tothetube.model.Arrival;
import com.example.tin.tothetube.model.StopPoint;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

// This class implements API connection by Retrofit2
// models used here are defined in "models" sub-package
// JSON is parsed with GSON library automatically
public class StopPointService {

    private static StopPointService sInstance;
    private static ApiContract sApiInstance;

    private StopPointService() {}

    public static StopPointService getInstance() {
        if (sInstance == null) {
            sInstance = new StopPointService();
        }
        return sInstance;
    }

    // this is a contract of API methods
    // we feed it to Retrofit and it generates a service for us
    public interface ApiContract {

        // root URL
        String baseUrl = "https://api.tfl.gov.uk";

        // https://api.tfl.gov.uk/StopPoint?lat=51.514&lon=-0.122&stopTypes=NaptanMetroStation&radius=1000&app_id={{app_id}}&app_key={{app_key}}
        @GET("StopPoint")
        Call<StopPoint.PaginatedResponse> getByRadius(@Query("lat") Double lat,
                                                      @Query("lon") Double lng,
                                                      @Query("stopTypes") String stopTypes,
                                                      @Query("radius") Integer radius,
                                                      @Query("app_id") String appId,
                                                      @Query("app_key") String appKey);

        // https://api.tfl.gov.uk/StopPoint/:id/Arrivals?app_id={{app_id}}&app_key={{app_key}}
        @GET("StopPoint/{id}/Arrivals")
        Call<List<Arrival>> getArrivalsById(@Path("id") String stopPointId,
                                            @Query("app_id") String appId,
                                            @Query("app_key") String appKey);
    }

    // builder of API service
    // we hide it from public access
    private ApiContract getApi() {

        if (sApiInstance != null) {
            return sApiInstance;
        }

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiContract.baseUrl)
                .client(client)
                // this line here attaches JSON parser to Retrofit service
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        sApiInstance = retrofit.create(ApiContract.class);
        return sApiInstance;
    }

    // this method is public
    // it decouples the public invocations from internal logic of API service
    public Call<StopPoint.PaginatedResponse> getByRadius(Double lat, Double lng, String stopType, Integer radius){
        return getApi().getByRadius(lat, lng, stopType, radius, BuildConfig.TFL_APP_ID, BuildConfig.TFL_APP_KEY);
    }

    // this method is public
    // it decouples the public invocations from internal logic of API service
    public Call<List<Arrival>> getArrivalsById(String stopPointId){
        return getApi().getArrivalsById(stopPointId, BuildConfig.TFL_APP_ID, BuildConfig.TFL_APP_KEY);
    }

}
