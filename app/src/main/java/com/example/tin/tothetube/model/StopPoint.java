package com.example.tin.tothetube.model;

import java.util.List;

public class StopPoint extends BaseModel {

    public String id;
    public String naptanId;
    public Line[] lines;
    public String commonName;
    public Double distance;

    // container for arrivalView data
    public List<Arrival> arrivals;

    // response of api call
    public static class PaginatedResponse {
        public List<StopPoint> stopPoints;
        public Integer pageSize;
        public Integer total;
        public Integer page;
    }

}
