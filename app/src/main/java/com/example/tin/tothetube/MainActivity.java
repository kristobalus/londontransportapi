package com.example.tin.tothetube;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tin.tothetube.model.Arrival;
import com.example.tin.tothetube.model.StopPoint;
import com.example.tin.tothetube.repositories.StopPointRepository;
import com.fasterxml.jackson.databind.util.StdDateFormat;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // preparing the views to be used to display the data
        // progress dialog is displayed while fetching data from API
        progressDialog = new ProgressDialog(this);

        // recycler view is a list of items
        recyclerView = findViewById(R.id.recycler_view);
        // we set the linear layout with vertical orientation, straight ordering
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        // here we start the background task which fetches the data, it is AsyncTask
        FetchTask task = new FetchTask();
        // the only way to pass multiple arguments into AsyncTask is a wrapper class, FetchParams
        task.execute(new FetchParams(51.514, -0.122, 1000));

        // for background logic see FetchTask implementation below....
    }

    // this class that wraps multiple arguments for FetchTask into a single object
    public static class FetchParams {

        // latitude
        Double lat;
        // longitude
        Double lng;
        // radius
        Integer radius;

        public FetchParams(Double lat, Double lng, Integer radius) {
            this.lat = lat;
            this.lng = lng;
            this.radius = radius;
        }
    }

    // main class for API background work
    public class FetchTask extends AsyncTask<FetchParams, Void, List<StopPoint>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // this method is executed on the main thread, before loading data
            // let's show progress dialog spinning
            progressDialog.setMessage("Loading...");
            progressDialog.show();
        }

        @Override
        protected List<StopPoint> doInBackground(FetchParams... args) {

            // this code is executed in the background
            // we do all the network calls here
            // since its background thread, we can allow blocking invocations
            // so just call the API method, wait for result, call the other method, wait for result...


            try {

                // first fetch stop point list
                FetchParams params = args[0];
                String stopType = "NaptanMetroStation";

                // StopPointRepository is an abstraction of the StopPointService
                // it decouples the networking logic from application logic
                List<StopPoint> points = StopPointRepository.getInstance().getStopPointList(params.lat, params.lng, stopType, params.radius);

                // then for each item in the stop point list we send request for arrivals
                for (StopPoint point : points) {
                    // arrivals are stored in the "arrivals" property of the stop point
                    point.arrivals = StopPointRepository.getInstance().getArrivalList(point.id);
                }

                // the list of stop points are the sought result,
                // it will be passed into onPostExecute() method
                return points;

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(List<StopPoint> result) {
            super.onPostExecute(result);

            // here we are! the result is a ArrayList of stop points, each has arrivals

            // since the background job is done,let's hide progress dialog
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            // and the show the list of points by
            // constructing adapter with the data and assigning it ot recyclerView
            Adapter adapter = new Adapter(result);
            recyclerView.setAdapter(adapter);
        }

    }



    // this is an adapter class for the RecyclerView
    // it is used together with ViewHolder class
    // adapter creates view of the row, wraps it into ViewHolder and populates the row with data
    public static class Adapter extends RecyclerView.Adapter<ViewHolder> {

        ArrayList<StopPoint> items = new ArrayList<>();

        public Adapter(List<StopPoint> items) {
            this.items.addAll(items);
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            // the view of the row is created here
            View v = LayoutInflater.from(parent.getContext()).inflate(ViewHolder.layoutId, parent, false);
            // ... and wrapped into view holder
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            // here is the binding magic happens
            StopPoint item = items.get(position);
            holder.bind(item);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }
    }

    // this is a view holder for the RecyclerView
    // it represents a cached row of recyclerView
    // it accounts for keeping the refs to Views and binding the data to the views
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public static int layoutId = R.layout.item_list;

        // parser of timestamp as it stored in API. it uses jackson package
        public static final StdDateFormat apiDateFormat = new StdDateFormat();
        // this is a formatter of timestamp, as it should be visible in the UI
        public static final SimpleDateFormat viewDateFormat = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z", Locale.US);

        public TextView pointNameView;
        public TextView arrivalView;

        public ViewHolder(View itemView) {
            super(itemView);
            // find view and store the ref in the viewholder
            // it helps to avoid repeating searches and saves CPU
            pointNameView = itemView.findViewById(R.id.point_name);
            arrivalView = itemView.findViewById(R.id.arrivals);
        }

        // this method binds the stop point to the row of recyclerView
        public void bind(StopPoint point) {

            pointNameView.setText(point.commonName);

            // sort the data before assigning
            // we sort it by ascending order
            Collections.sort(point.arrivals, new Comparator<Arrival>() {
                @Override
                public int compare(Arrival arrival1, Arrival arrival2) {

                    try {
                        Date d1 = apiDateFormat.parse(arrival1.expectedArrival);
                        Date d2 = apiDateFormat.parse(arrival2.expectedArrival);
                        return d1.compareTo(d2);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return 0;
                }
            });

            // forming lines of arrival information
            // this is for illustration purposes only
            StringBuilder sb = new StringBuilder();
            int max = Math.min(3, point.arrivals.size());
            for(int i = 0; i < max ; i++){
                Arrival arrival = point.arrivals.get(i);
                try {
                    Date o = apiDateFormat.parse(arrival.expectedArrival);
                    sb.append(viewDateFormat.format(o));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                sb.append("\n");
            }

            // assigning arrival data to the view
            arrivalView.setText(sb.toString());
        }
    }


}
